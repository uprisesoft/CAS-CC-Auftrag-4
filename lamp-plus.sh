#!/bin/bash
apt-get -y update

# set up a silent install of MySQL
dbpass=$1

locale-gen de_CH.utf8
update-locale LANG=de_CH.UTF-8 LC_MESSAGES=POSIX

export DEBIAN_FRONTEND=noninteractive
echo mysql-server mysql-server/root_password password $dbpass | debconf-set-selections
echo mysql-server mysql-server/root_password_again password $dbpass | debconf-set-selections
echo phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2 | debconf-set-selections
echo phpmyadmin phpmyadmin/dbconfig-install boolean true | debconf-set-selections
echo phpmyadmin phpmyadmin/mysql/admin-user string root | debconf-set-selections
echo phpmyadmin phpmyadmin/mysql/admin-pass password $dbpass | debconf-set-selections
echo phpmyadmin phpmyadmin/mysql/app-pass password $dbpass |debconf-set-selections
echo phpmyadmin phpmyadmin/app-password-confirm password $dbpass | debconf-set-selections

# install the LAMP stack
apt-get -y install apache2 mysql-server php5 php5-mysql build-essential bsdtar curl wget git-core python-software-properties unzip phpmyadmin

# write some PHP
echo \<center\>\<h1\>CAS Cloud Computing Auftrag 4\</h1\>\<br/\>\</center\> > /var/www/html/phpinfo.php
echo \<\?php phpinfo\(\)\; \?\> >> /var/www/html/phpinfo.php

# restart Apache
apachectl restart